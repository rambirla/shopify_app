<?php
include 'header.php';

$new = $shopify('GET /admin/api/2021-10/products.json?');


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>JSON Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
<style>


</style>


  </head>

<body>

    <div class="container">
        <h3 style="padding-top: 20px;">Products</h3>

        <div>
            <div>
                <input type="text" id="searchText" onkeyup="showData()"/>
                <button style="padding: 3px 12px" type="button" onclick="showData()" class="btn btn-primary">
                    <i class="fas fa-search"></i>
                </button>
            </div>

        </div>

        <div id="tabledata" style="padding-top: 20px;">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Status</th>
                        <th>Inventory</th>
                        <th>Vendor</th>
                        <th>Action</th>
                       
                    </tr>
                </thead>
                <?php foreach ($new as $key => $value) {
                ?>
                    <tbody id="show_data">
                        <tr id="pro<?= $value['id']?>">
                            <td><img width="30px" style="margin-right: 10px;" src="<?php echo $value['images'][0]['src'] . "<br>"; ?>"></img><a style="color: black" href="#"><?php echo $value['title'] . "<br>"; ?></a></td>
                            <td><?php echo $value['status']; ?></td>
                            <td><?php echo $value['variants'][0]['inventory_quantity'] . " in stock"; ?></td>
                            <td><?php echo $value['vendor']; ?> </td>
                            <td><button type="button" data-toggle="modal" data-target="#exampleModal"  onclick="updateRow(this.id)" id="<?php echo $value['id'];?>">Update</button>
                            <button onclick="deleteRow(this.id)" id="<?php echo $value['id'];?>">Delete</a></button></td>
                        
                          </tr>
                    </tbody>
                <?php } ?>
            </table>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" > 
        <form  enctype='multipart/form-data' id="myform">
            <div class="form-group" >
           <input type="hidden" id="product_id" value="">
            <label for="image" class="col-form-label">Image</label><br> 
            <img width="50px" style="margin-right:10px;" id="m_image" src=""></img>
                </div>
              <div> 
               <input type="file" name="product_image" id="image"> 
              </div>
            <div class="form-group">
            <label for="product" class="col-form-label">Product</label>
            <input type="text" class="form-control" name="title" id="m_product" value=" ">
          </div>
          <div class="form-group">
            <label for="status" class="col-form-label">Status:</label>
            <input type="text" class="form-control" name="status" id="m_status" value=" ">
          </div>
          <div class="form-group">
            <label for="inventory" class="col-form-label">Inventory:</label>
            <input type="text" class="form-control" name="inventory" id="m_inventory" value=" ">
          </div>
          <div class="form-group">
            <label for="vendor" class="col-form-label">Vendor:</label>
            <input type="text" class="form-control" name="vendor" id="m_vendor" value=" ">
          </div>
               
          <div class="container pt-4">
    <div class="table-responsive">
      <table class="table table-bordered">
    
        <tbody id="tbody">
        </tbody>
      </table>
    </div>
    <button class="btn btn-md btn-primary" 
      id="addBtn" type="button">
        Add Variant
    </button>
  </div>         
              
    
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  name="submit"  class="btn btn-primary updateData" id="">Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

        <script>
        function showData() {
            var searchText = document.getElementById("searchText").value;
            //console.log(searchText);

            const xhttp = new XMLHttpRequest();
            xhttp.onload = function() {
                var json = JSON.parse(this.responseText)
                           
                jQuery(json).each(function(index,data){
                  var title= data.title.toLowerCase()
                  if(title.includes(searchText.toLowerCase())){
                    display_html(data)
                  }
                                     

                })
            }
            xhttp.open("GET", "<?=SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com");
            xhttp.send();
        }

        function display_html(data){
            var html = '<tr> <td><img width="30px" style="margin-right: 10px;" src="'+data.images[0].src+'"></img><a style="color: black" href="#">'+data.title+'</a></td> <td>'+data.status+'</td> <td>'+data.variants[0].inventory_quantity+'</td> <td>'+data.product_type+'</td> <td>'+data.vendor+' </td> </tr>'
            jQuery('#show_data').append(html)
        }


        function deleteRow(id) {
          var productId=id;
          console.log(productId)
        $.ajax({

          type:"GET",
          url:"<?=SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com&page=delete_pro&product_id=" + productId,
          dataType:"json",
          success: function(data) {
            console.log(data)
          $("#pro" + productId).remove();
        }
        });
      }



        function updateRow(id){
        var productId=id;
        $.ajax({

          type:"GET",
          url:"<?=SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com&page=get_data&product_id=" + productId,
          dataType:"json",
          success: function(data) {
            // jQuery('#newData tr').remove();
            console.log(data)
            modalData(data)
          }
            });
          
          }


            function modalData(data){  
          var imageSrc;
          try{
            imageSrc = data.images[0].src;
          }catch(err){
            imageSrc = ''
          }
               
          var name= data.title;
          var status=data.status;
          var inventory=data.variants[0].inventory_quantity;
          var vendor=data.vendor;
          jQuery('.updateData').attr('id',data.id)
          document.getElementById('m_image').src=imageSrc;
          document.getElementById('m_product').value=name;
          document.getElementById('m_status').value=status;
          document.getElementById('m_inventory').value=inventory;
          document.getElementById('m_vendor').value=vendor;
          // console.log(image);
             
   }
      function display_html(data){
            var html = ' <tr> <td><img width="30px" style="margin-right: 10px;" src="'+data.images[0].src+'"></img><a style="color: black" href="#">'+data.title+'</a></td> <td>'+data.status+'</td> <td>'+data.variants[0].inventory_quantity+'</td> <td>'+data.product_type+'</td> <td>'+data.vendor+' </td> </tr>'
            jQuery('#show_data').append(html)

      }

        $('#myform').submit(function(e) {
                e.preventDefault();
                var product_id = $('.updateData').attr('id');
                console.log(product_id)
                  // console.log(id)
                  $.ajax({
                    type: 'POST',
                    url:'<?php echo SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com&page=put_data&product_id=' + product_id,
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(res) {
                        console.log(res)
                        alert('form was submitted');
                    }
                });
              

            });

            $(document).ready(function () {
  

  var rowId = 0;


  $('#addBtn').on('click', function () {

    $('#tbody').append(`<tr id="R${++rowId}">
         <td class=row">
         <td><input type="text"  name="color[]" id=""  value="" placeholder="Color"></td>
         <td><input type="text"  name="price[]" id="" value=""  placeholder="Price"></td>
       <td><input type="file" name="v_image[]"></td>
        
          </tr>`);
  });


    });






//             function inventoryUpdate(data){
//               var product_id=id;

//               $.ajax({
//               type:"GET",
//               url:"<?=SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com&page=inventory_data&inventory_item_id=" + $inventory_item_id,
//               dataType:"json",
//               success: function(data) {
//               console.log(data)
// }
//   });

// }

            




        //     function updateData(id) {
        //     var product_id = id;
        //     console.log(id)
        //     var m_product = document.getElementById('m_product').value;
        //     var m_status = document.getElementById('m_status').value;
        //     var m_inventory = document.getElementById('m_inventory').value;
        //     var m_vendor = document.getElementById('m_vendor').value;

        //     console.log(product_id);
        //     console.log(m_product);
        //     console.log(m_status);
        //     console.log(m_inventory);
        //     console.log(m_vendor);
        

        //     const xhttp = new XMLHttpRequest();
        //     xhttp.onload = function() {
        //         var json = this.responseText;
        //         console.log(json);
        //     }



        //     xhttp.open("POST","<?php echo SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com&page=put_data&product_id=" + product_id);
        //     console.log(product_id);
        //     var data = {
        //         title: m_product,
        //         status: m_status,
        //         inventory: m_inventory,
        //         vendor: m_vendor
        //     };

           
        //     xhttp.send(JSON.stringify(data));

          
        // }
      
    //   function updateData(id){

    //     var productId=id;


    //     var mName=document.getElementById('m_product').value=mName;
    //     var mStatus=document.getElementById('m_status').value=mStatus;
    //     var minventory=document.getElementById('m_inventory').value=minventory;
    //     var mVendor=document.getElementById('m_vendor').value=mVendor;
       

    //     $.ajax({

    //       type:"PUT",
    //       url:"<?=SHOPIFY_SITE_URL ?>product.php?oauth_token=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&shop=demo-app-ecom.myshopify.com&page=put_data&product_id=" + productId,
    //       dataType:"json",
    //        data: {name:'tech',status: 'active',inventory:'5',vendor:'ecom '},

    //       success: function(data) {
          
    //         console.log(data)
  
    //     }
    //   });
    // }
  
</script>

</body>

</html>
