// Show popup on Add to cart //
var page ;
var local = location.href
if(local.includes('product')){
 page = 'product'
}else if(local.includes('cart')){
page = 'cart'
}


// get shop name //
var shop = Shopify.shop


//  Shop Pop up on exit intent //
document.addEventListener("mouseleave", function(e) {
    if (e.clientY < 0) {
        if (Number(sessionStorage.exit_init) != 1) {
          if(page == 'cart' || page == 'product'){
                    	sessionStorage.exit_init = 1;
            escrm_html()
          }
        }
    }
}, false);

// Showing popup on click Add to cart button //
 $('button[type="submit"]').each(function(index,data){
    if($(this).text().includes("Add to cart")){
      console.log("check")
        $(this).addClass("escrm_custom_classname")    
    }
})

$(".escrm_custom_classname").click(function(){
  escrm_html()
}) 

// Dynamic css Append//
function escrm_css_append(){

    var data = document.createElement("link")
    data.rel = 'stylesheet'
    data.type = 'text/css'
    data.href = 'https://49d0-103-21-55-66.ngrok.io/shopify_apps/popup_style.css'
    document.getElementsByTagName('head')[0].appendChild(data);

}
escrm_css_append()


// Display login popup  modal
function escrm_html() {

    var escrm_html = '<div id="escrm_form" class="container escrm_form escrm_modal"> <div class="row justify-content-center escrm_form_inner overlay escrm_login_box" id="escrm_popup" > <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2"> <div class="card px-0 pt-4 pb-0 mt-3 mb-3"> <h2 id="escrm_heading">Join The Club</h2> <p>Easily View & Manage Your Order And Unlock Offers And Discount!</p> <a id="escrm_btnClosePopup" onclick="escrm_close_modal(this)" class="close eshop_crm_modal" href="#">&times;</a> <form id="escrm_form" method="POST"> <!-- form start --> <fieldset class="escrm_email_box"> <div class="form-card"> <input id="escrm_email_login" class="escrm_email" type="text" name="escrm_email" placeholder="Email" /> </div> <input id="escrm_next" type="button" name="escrm_next" onclick="escrm_validateEmail()" class="escrm_next action-button" value="next" /> <div class="escrm_cmn-btn escrm_btn_lft"> <button type="button"><i class="fa fa-facebook" aria-hidden="true"></i>Sign up with facebook</button> </div> <div class="escrm_cmn-btn escrm_btn_rgt"> <button type="button"><img class="escrm_google_logo" src="https://cdn.shopify.com/s/files/1/0605/4540/1022/t/1/assets/Google_Logo.png?v=1640667766">Sign up Google</button> </div> <div class="escrm_have_account"> <a href="https://anshul-eshopcrm.myshopify.com/account/login">Have A Account? Sign In</a> <span id="escrm_skip" class="escrm_skip_btn eshop_crm_modal" onclick="escrm_close_modal(this)">skip</span> </div> </fieldset> <fieldset class="escrm_password_box"> <div class="form-card"> <input id="escrm_password" type="password" name="escrm_password" placeholder="Password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required /> </div> <input id="escrm_save_btn" type="button" onclick="escrm_validatePassword()" name="escrm_next" class="escrm_next action-button" value="Save & Continue" /> <div class="escrm_have_account"> <span id="escrm_back" onclick="escrm_previous()"> >>back</span> <span class="escrm_skip_btn eshop_crm_modal">skip</span> </div> </fieldset> </form> <!-- end form --> </div> </div> </div> </div>'

    var data = document.createElement("div")
    data.id = 'checking_html'
    data.innerHTML = escrm_html
    document.getElementsByTagName('body')[0].appendChild(data);
}

if(page == 'cart'){
escrm_html()
}

// close popup //

function escrm_close_modal(e){
 $('#checking_html').remove();
  escrm_createCookie("popup_cookie", 30, 30)
}

// back button functionality //
function escrm_previous() {
 $('.escrm_email_box').show();
 $('.escrm_password_box').hide();
}


// email validation //
function escrm_validateEmail() {
	$(".escrm_error").hide();
    var hasError = false;
    var reg = /^([A-Za-z0-9_\-\.]){1,}\@([A-Za-z0-9_\-\.]){1,}\.([A-Za-z]{2,4})$/;
    var address = document.getElementById('escrm_email_login').value;
    if (!reg.test(address)) {
        $('.escrm_error').remove()
        $("#escrm_email_login").after('<span class="escrm_error" style="color:red">Please enter correct email address.</span>');
        hasError = true;
    }

    if (reg.test(address) == true) {
        $('.escrm_password_box').show();
        $('.escrm_email_box').hide();
    }
}


// password validation //
 function escrm_validatePassword() {
    $(".escrm_error").hide();
    var hasError = false;
    var InputValue = $("#escrm_password").val();
    var regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    if (!regex.test(InputValue)) {
        $('.escrm_error').remove()
        $("#escrm_password").after('<span class="escrm_error" style="color:red">Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters</span>');
        hasError = true;
    } else {
        alert('valid password');
        var email = $('#escrm_email_login').val()
        var password = $('#escrm_password').val()

        $.ajax({
            type: "POST",
            url: 'https://dev.eshopcrm.com/api/' + shop + '/customers/register',
            dataType: "json",
            data: {
                email: email,
                password: password
            },
            success: function(data) {
                $('#escrm_popup').hide();
                console.log(data)
            }
        });
    }
}

// create cookie //

function escrm_createCookie(name, value, minutes) {
    var date = new Date();
    var time = date.setTime(date.getTime() + (minutes * 60 * 1000));
    var expires = "; expires=" + date.toGMTString();
    document.cookie = name + "=" + value + expires + (30 * 60 * 1000);
    path = "/";
}

var escrm_cookies = document.cookie.split(';').map(cookie => cookie.split('='))
    .reduce((accumulator, [key, value]) => ({
        ...accumulator,
        [key.trim()]: decodeURIComponent(value)
    }), {});
var cookie_value = escrm_cookies.popup_cookie
console.log(cookie_value)

if (cookie_value == undefined) {
    document.getElementByClass('escrm_login_box').classList.add('escrm_close');
    document.getElementById('#escrm_popup').classList.add('escrm_login_box');
    document.getElementById('#escrm_popup').classList.remove('escrm_close');
    console.log("checking")

} else {
    document.getElementById('#escrm_popup').classList.remove('escrm_login_box');
}




  